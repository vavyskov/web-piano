(() => {
    let keys = ['D', 'F', 'G', 'H', 'J', 'K', 'L', 'R', 'T', 'U', 'I', 'O'];
    let audio = new Audio();
    let message;
    let key;

    // Keyboard event
    document.addEventListener('keydown', pressEvent)
    document.addEventListener('keyup', releaseEvent)

    function pressEvent(event) {
        // Disable repeating (long key holding)
        //console.log(event.repeat);
        if (event.repeat) return;

        key = event.key.toUpperCase();
        playSound(key);
        addActiveClass(key);
    }

    function releaseEvent(event) {
        removeActiveClass(key);
    }

    function playSound(key) {
        if (keys.includes(key)) {
            // console.log("The '" + event.key + "' key is pressed.");
            message = `The "${key}" key has been activated.`;

            audio.src = `assets/audio/${key}.mp3`;
            audio.play();
        }

        console.log(message);
    }

    function addActiveClass(key) {
        for (const kbd of document.querySelectorAll(".piano-key")) {
            if (kbd.textContent.includes(key)) {
                kbd.classList.add('active');
            }
        }
    }

    function removeActiveClass(key) {
        for (const kbd of document.querySelectorAll(".piano-key")) {
            if (kbd.textContent.includes(key)) {
                kbd.classList.remove('active');
            }
        }
    }
})();
