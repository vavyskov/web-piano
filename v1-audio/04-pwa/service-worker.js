/**
 * PWA: Service Worker je JavaScript, který je spuštěn (běží v prohlížeči) i v době, kdy uživatel na webu zrovna není.
 * (off-line režim, rozesílání push notifikací atd.)
 */

// Uložení offline dat
var staticCacheName = "04-pwa-cache-store";

self.addEventListener('install', (event) => {
  event.waitUntil(
    caches.open(staticCacheName).then((cache) => cache.addAll([
      '/04-pwa/',
      '/04-pwa/index.html',
      '/04-pwa/js/howler.core.min.js',
      '/04-pwa/js/pwa.js',
      '/04-pwa/js/script.js',
      '/04-pwa/css/style.css',
      '/04-pwa/assets/icons/icon-48x48.ico',
      '/04-pwa/assets/icons/icon-192x192-background-maskable.png',
      '/04-pwa/assets/icons/icon-192x192.png',
      '/04-pwa/assets/icons/icon-512x512.png',
      '/04-pwa/assets/icons/icon-maskable.svg',
      '/04-pwa/assets/icons/icon.svg',
      '/04-pwa/assets/audio/D.mp3',
      '/04-pwa/assets/audio/F.mp3',
      '/04-pwa/assets/audio/G.mp3',
      '/04-pwa/assets/audio/H.mp3',
      '/04-pwa/assets/audio/I.mp3',
      '/04-pwa/assets/audio/J.mp3',
      '/04-pwa/assets/audio/K.mp3',
      '/04-pwa/assets/audio/L.mp3',
      '/04-pwa/assets/audio/O.mp3',
      '/04-pwa/assets/audio/R.mp3',
      '/04-pwa/assets/audio/T.mp3',
      '/04-pwa/assets/audio/U.mp3',
    ])),
  );
});

self.addEventListener('fetch', (event) => {
  //console.log(event.request.url);
  event.respondWith(
    caches.match(event.request).then((response) => response || fetch(event.request)),
  );
});
