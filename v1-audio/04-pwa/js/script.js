(() => {
    let keys = ['D', 'F', 'G', 'H', 'J', 'K', 'L', 'R', 'T', 'U', 'I', 'O'];
    let audio = new Howl({
        src: ['assets/audio/D.mp3'],
        // Don`t wait for the full file to be downloaded and decoded before playing
        html5: true,
        // Playback started
        onplay: function () {
            //console.log("Playback started.");
        },
        // Playback failed
        onplayerror: function (error) {
            //console.error(error);
        }
    });
    let message;
    let key;

    // Keyboard event
    document.addEventListener('keydown', pressEvent)
    document.addEventListener('keyup', releaseEvent)

    // Mouse event (version A)
    /*
    const pianoKeys = document.querySelectorAll(".piano-key");
    pianoKeys.forEach(function (pianoKey) {
        pianoKey.addEventListener("click", function (event) {
            key = event.target.innerText;
            playSound(key);
        });
    });
    */

    // Mouse event (version B)
    document.addEventListener('mousedown', pressEvent)
    document.addEventListener('mouseup', releaseEvent)

    // Touch event
    document.addEventListener('touchstart', pressEvent)
    document.addEventListener('touchend', releaseEvent)

    function pressEvent(event) {
        if (event.type === "keydown") {
            // Disable repeating (long key holding)
            //console.log(event.repeat);
            if (event.repeat) return;

            key = event.key.toUpperCase();
        }
        if (event.target.classList.contains('piano-key')) {
            if (event.type === "touchstart") {
                event.preventDefault();
            }
            key = event.target.innerText;
        }

        playSound(key);
        addActiveClass(key);
    }

    function releaseEvent(event) {
        if (event.target.classList.contains('piano-key')) {
            if (event.type === "touchend") {
                event.preventDefault();
            }
            key = event.target.innerText;
        }
        removeActiveClass(key);
    }

    function playSound(key) {
        if (keys.includes(key)) {
            // console.log("The '" + event.key + "' key is pressed.");
            message = `The "${key}" key has been activated.`;

            // Flush memory and load the new sound
            audio.unload();
            audio._src = `assets/audio/${key}.mp3`;
            audio.load();

            audio.play();
        } else {
            shakeX();
            message = "Some unbound key has been activated.";
        }

        //console.log(message);
    }

    function addActiveClass(key) {
        for (const kbd of document.querySelectorAll(".piano-key")) {
            if (kbd.textContent.includes(key)) {
                kbd.classList.add('active');
            }
        }
    }

    function removeActiveClass(key) {
        for (const kbd of document.querySelectorAll(".piano-key")) {
            if (kbd.textContent.includes(key)) {
                kbd.classList.remove('active');
            }
        }
    }

    /* The shake effect */
    function shakeX() {
        // document.querySelector('.piano').classList.toggle("shakeX");
        document.querySelector('.piano').animate([
            { transform: 'translate3d(-1px, 0, 0)' },
            { transform: 'translate3d(2px, 0, 0)' },
            { transform: 'translate3d(-4px, 0, 0)' },
            { transform: 'translate3d(4px, 0, 0)' },
            { transform: 'translate3d(-4px, 0, 0)' },
            { transform: 'translate3d(4px, 0, 0)' },
            { transform: 'translate3d(-4px, 0, 0)' },
            { transform: 'translate3d(2px, 0, 0)' },
            { transform: 'translate3d(-1px, 0, 0)' },
        ], {
            duration: 500,
        });
    }

})();
