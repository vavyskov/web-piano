/**
 * PWA: Service Worker je JavaScript, který je spuštěn (běží v prohlížeči) i v době, kdy uživatel na webu zrovna není.
 * (off-line režim, rozesílání push notifikací atd.)
 */

// Uložení offline dat
var staticCacheName = "03-pwa-cache-store";

self.addEventListener('install', (event) => {
  event.waitUntil(
    caches.open(staticCacheName).then((cache) => cache.addAll([
      '/03-pwa/',
      '/03-pwa/index.html',
      '/03-pwa/js/pwa.js',
      '/03-pwa/js/script.js',
      '/03-pwa/css/style.css',
      '/03-pwa/assets/icons/icon-48x48.ico',
      '/03-pwa/assets/icons/icon-192x192-background-maskable.png',
      '/03-pwa/assets/icons/icon-192x192.png',
      '/03-pwa/assets/icons/icon-512x512.png',
      '/03-pwa/assets/icons/icon-maskable.svg',
      '/03-pwa/assets/icons/icon.svg',
      '/03-pwa/assets/audio/D.mp3',
      '/03-pwa/assets/audio/F.mp3',
      '/03-pwa/assets/audio/G.mp3',
      '/03-pwa/assets/audio/H.mp3',
      '/03-pwa/assets/audio/I.mp3',
      '/03-pwa/assets/audio/J.mp3',
      '/03-pwa/assets/audio/K.mp3',
      '/03-pwa/assets/audio/L.mp3',
      '/03-pwa/assets/audio/O.mp3',
      '/03-pwa/assets/audio/R.mp3',
      '/03-pwa/assets/audio/T.mp3',
      '/03-pwa/assets/audio/U.mp3',
    ])),
  );
});

self.addEventListener('fetch', (event) => {
  //console.log(event.request.url);
  event.respondWith(
    caches.match(event.request).then((response) => response || fetch(event.request)),
  );
});
