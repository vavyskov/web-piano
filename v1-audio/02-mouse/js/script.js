(() => {
    let keys = ['D', 'F', 'G', 'H', 'J', 'K', 'L', 'R', 'T', 'U', 'I', 'O'];
    let audio = new Audio();
    let message;
    let key;

    // Keyboard event
    document.addEventListener('keydown', pressEvent)
    document.addEventListener('keyup', releaseEvent)

    // Mouse event
    document.addEventListener('mousedown', pressEvent)
    document.addEventListener('mouseup', releaseEvent)

    function pressEvent(event) {
        if (event.type === "keydown") {
            // Disable repeating (long key holding)
            //console.log(event.repeat);
            if (event.repeat) return;

            key = event.key.toUpperCase();
        }
        if (event.target.classList.contains('piano-key')) {
            key = event.target.innerText;
        }

        playSound(key);
        addActiveClass(key);
    }

    function releaseEvent(event) {
        if (event.target.classList.contains('piano-key')) {
            key = event.target.innerText;
        }
        removeActiveClass(key);
    }

    function playSound(key) {
        if (keys.includes(key)) {
            // console.log("The '" + event.key + "' key is pressed.");
            message = `The "${key}" key has been activated.`;

            audio.src = `assets/audio/${key}.mp3`;
            audio.play()
                .then(() => {
                    // Playback started
                    //console.log("Playback started.");
                })
                .catch((error) => {
                    // Playback failed
                    //console.error(error);
                })
            ;
        } else {
            shakeX();
            message = "Some unbound key has been activated.";
        }

        console.log(message);
    }

    function addActiveClass(key) {
        for (const kbd of document.querySelectorAll(".piano-key")) {
            if (kbd.textContent.includes(key)) {
                kbd.classList.add('active');
            }
        }
    }

    function removeActiveClass(key) {
        for (const kbd of document.querySelectorAll(".piano-key")) {
            if (kbd.textContent.includes(key)) {
                kbd.classList.remove('active');
            }
        }
    }

    /* The shake effect */
    function shakeX() {
        // document.querySelector('.piano').classList.toggle("shakeX");
        document.querySelector('.piano').animate([
            { transform: 'translate3d(-1px, 0, 0)' },
            { transform: 'translate3d(2px, 0, 0)' },
            { transform: 'translate3d(-4px, 0, 0)' },
            { transform: 'translate3d(4px, 0, 0)' },
            { transform: 'translate3d(-4px, 0, 0)' },
            { transform: 'translate3d(4px, 0, 0)' },
            { transform: 'translate3d(-4px, 0, 0)' },
            { transform: 'translate3d(2px, 0, 0)' },
            { transform: 'translate3d(-1px, 0, 0)' },
        ], {
            duration: 500,
        });
    }

})();
