# WEB Piano

Instalace PWA aplikace z prohlížeče:
- Windows:
    - IE, Edge, Chrome:
        - tlačítkem v aplikaci
- Linux:
    - ???
- macOS:
    - Safari:
        - tlačítkem v aplikaci
- iOS:
    - Safari:
        - kliknout na tlačítko "sdílení" ve spodní části okna prohlížeče zvolit "Add to Home Screen"
- Android:
    - Chrome:
        - tlačítkem v aplikaci

Odinstalace PWA aplikace ze zařízení:
- Windows:
    - IE:
        - Ovládací panely -> Programy -> Odinstalovat
    - Edge:
        - edge://apps
    - Chrome:
        - chrome://apps
- Linux:
    - ???
- iOS (Safari), Android (Chrome):
        - přidržením prstu na aplikaci na ploše

Volba technického řešení:
1. [HTML element &lt;audio&gt;](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/audio): přehrávání předem nahraných zvuků
1. Dynamické generování .wav souborů, převod do base64 a přehrávání pomocí &lt;audio&gt;:
    - návody
        - [How to Build a Piano Keyboard Using Vanilla JavaScript](https://www.freecodecamp.org/news/javascript-piano-keyboard/)
    - knihovny
        - [keithwhor-audiosynth-packaged](https://www.npmjs.com/package/keithwhor-audiosynth-packaged) - napodobení "reálného" zvuku (klavír, varhany, kytara atd.)
        - [Ember-cli-guitar-chords](https://www.npmjs.com/package/ember-cli-guitar-chords) - akordy
1. [JavaScript rozhraní AudioContext()](https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API/Simple_synth): dynamické generování zvuků (zvukový syntetizátor)

Tipy:
- https://mrcoles.com/piano/ (nastavení, demo melodie)
- https://www.charmingdesk.com.ng/2021/06/20/how-to-build-a-piano-keyboard-with-audiosynth/
- https://www.charmingdesk.com.ng/2021/05/29/how-to-create-a-javascript-piano/
- https://dev.to/mizadmehr/let-s-create-a-song-with-vanilla-javascript-54ec

Kontrola:
- Google Chrome:
    - DevTools: **Lighthouse** (spouštět v **anonymním okně** nebo vypnout všechna rozšíření)
        - [Node.js](https://nodejs.org/en/)
            - `npx http-server`
            - `npx http-server -c31536000` (cache time in seconds)
    - Rozšíření: **Web Vitals**

Poznámky:
- PWA aplikace je nakonfigurována tak, že na webu funguje v podadresáři "03-pwa" (lokální server např. `npx http-server` spustit v adresáři `01-audio` a přejít na adresu `http://127.0.0.1:8080/03-pwa/`).

Howler.js:
- odstranění "zdvojeného" začátku zvuku v Safari

ToDo:
- Popisky kláves (klávesnice + názvy not)
- Responzivní design (procentuální velikost klávesnice dle šířky displeje)
- Horizontální posun víceoktávové klávesnice (např. pomocí šipek vpravo/vlevo)
- Zakázat zvětšování pomocí rychlého dotyku na dotykových zařízeních (iOS Safari 15+):
    - nefunguje:
        - html:
            - `<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">`
        - css:
            - 
            ```
            kbd {
                touch-action: pan-y;
                touch-action: manipulation;
            }
            ```
        - js:
            - 
            ```
            document.addEventListener('touchend', function (event) {
                if (event.target.classList.contains('kbd')) {
                    event.preventDefault();
                }
            }, false);
            ```
            -
            ```
            let doubleTouchStartTimestamp = 0;
            let maxDelayBetweenTaps = 500;
            document.addEventListener("touchstart", function (event) {
                let now = +(new Date());
                if (doubleTouchStartTimestamp + maxDelayBetweenTaps > now) {
                    event.preventDefault();
                };
                doubleTouchStartTimestamp = now;
            });
            ```
- Delší stisk na dotykovém zařízení:
    - zakázat výběru textu
    - zakázat překlad

Tips:
- přepínání nástrojů a vzhledu (klavír, kytara, bubny)
